package com.example.zavrsni_rad;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.zavrsni_rad.Models.Check;
import com.example.zavrsni_rad.Models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    @BindView(R.id.etPersonName)
    EditText etPersonName;
    @BindView(R.id.etSedimentcaijaErit)
    EditText etSedimentcaijaErit;
    @BindView(R.id.bConfirm)
    Button bConfirm;
    @BindView(R.id.spGender)
    Spinner spGender;
    @BindView(R.id.etAge)
    EditText etAge;
    @BindView(R.id.etLeukociti)
    EditText etLeukociti;
    @BindView(R.id.etEritrociti)
    EditText etEritrociti;
    @BindView(R.id.etHemoglobin)
    EditText etHemoglobin;
    @BindView(R.id.etHematokrit)
    EditText etHematokrit;
    @BindView(R.id.etMCH)
    EditText etMCH;
    @BindView(R.id.etMCHC)
    EditText etMCHC;
    @BindView(R.id.etMCV)
    EditText etMCV;
    @BindView(R.id.etTrombociti)
    EditText etTrombociti;
    @BindView(R.id.etRDW)
    EditText etRDW;
    @BindView(R.id.etProsjecniVolTrombocita)
    EditText etProsjecniVolTrombocita;
    @BindView(R.id.etGlukoza)
    EditText etGlukoza;
    @BindView(R.id.etUrea)
    EditText etUrea;
    @BindView(R.id.etKreatinin)
    EditText etKreatinin;
    @BindView(R.id.etAST)
    EditText etAST;
    @BindView(R.id.etALT)
    EditText etALT;
    @BindView(R.id.etGGT)
    EditText etGGT;

    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);
        ButterKnife.bind(this);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Gender, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(adapter);
        spGender.setOnItemSelectedListener(this);

        bConfirm.setOnClickListener(this);

    }

    private void setEditingEnabled(boolean enabled) {
        etPersonName.setEnabled(enabled);
        etSedimentcaijaErit.setEnabled(enabled);
        etLeukociti.setEnabled(enabled);

        etEritrociti.setEnabled(enabled);

        etHemoglobin.setEnabled(enabled);

        etHematokrit.setEnabled(enabled);

        etMCH.setEnabled(enabled);

        etMCHC.setEnabled(enabled);

        etMCV.setEnabled(enabled);

        etTrombociti.setEnabled(enabled);

        etRDW.setEnabled(enabled);

        etProsjecniVolTrombocita.setEnabled(enabled);

        etGlukoza.setEnabled(enabled);

        etUrea.setEnabled(enabled);

        etKreatinin.setEnabled(enabled);

        etAST.setEnabled(enabled);

        etALT.setEnabled(enabled);

        etGGT.setEnabled(enabled);

        spGender.setEnabled(enabled);
        etAge.setEnabled(enabled);

        if (enabled) {
            bConfirm.setVisibility(View.VISIBLE);
        } else {
            bConfirm.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String selected = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void writeNewCheck(String userId, User user, Check check) {

        String key = databaseReference.child("checks").push().getKey();
        Map<String, Object> postValues = check.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/checks/" + key, postValues);
        childUpdates.put("/user-checks/" + userId + "/" + key, postValues);

        databaseReference.updateChildren(childUpdates);
    }

    @Override
    public void onClick(View view) {
        final String userId = getUid();
        setEditingEnabled(true);


        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String datum = dateFormat.format(currentTime);

        String personName = etPersonName.getText().toString();

        String spol = spGender.getSelectedItem().toString();

        String dob = etAge.getText().toString();
        if (TextUtils.isEmpty(dob)){etAge.setError("Unos obavezan");return;}
        if (Integer.valueOf(dob)<20){ etAge.setError("Osoba mora biti starija od 20 godina");return; }
        String sedimantacijaEritrocita = etSedimentcaijaErit.getText().toString();if (TextUtils.isEmpty(sedimantacijaEritrocita)){etSedimentcaijaErit.setError("Unos obavezan");return;}

        String leukociti = etLeukociti.getText().toString();if (TextUtils.isEmpty(leukociti)){etLeukociti.setError("Unos obavezan");return;}

        String eritrociti= etEritrociti.getText().toString();if (TextUtils.isEmpty(eritrociti)){etEritrociti.setError("Unos obavezan");return;}

        String hemoglobin= etHemoglobin.getText().toString();if (TextUtils.isEmpty(hemoglobin)){etHemoglobin.setError("Unos obavezan");return;}

        String hematokrit = etHematokrit.getText().toString();if (TextUtils.isEmpty(hematokrit)){etHematokrit.setError("Unos obavezan");return;}

        String mch = etMCH.getText().toString();if (TextUtils.isEmpty(mch)){etMCH.setError("Unos obavezan");return;}

        String mchc = etMCHC.getText().toString();if (TextUtils.isEmpty(mchc)){etMCHC.setError("Unos obavezan");return;}

        String mcv = etMCV.getText().toString();if (TextUtils.isEmpty(mcv)){etMCV.setError("Unos obavezan");return;}

        String trombociti = etTrombociti.getText().toString();if (TextUtils.isEmpty(trombociti)){etTrombociti.setError("Unos obavezan");return;}

        String rdw = etRDW.getText().toString();if (TextUtils.isEmpty(rdw)){etRDW.setError("Unos obavezan");return;}

        String voltrombocita = etProsjecniVolTrombocita.getText().toString();if (TextUtils.isEmpty(voltrombocita)){etProsjecniVolTrombocita.setError("Unos obavezan");return;}

        String glukoza = etGlukoza.getText().toString();if (TextUtils.isEmpty(glukoza)){etGlukoza.setError("Unos obavezan");return;}

        String urea = etUrea.getText().toString();if (TextUtils.isEmpty(urea)){etUrea.setError("Unos obavezan");return;}

        String kreatinin = etKreatinin.getText().toString();if (TextUtils.isEmpty(kreatinin)){etKreatinin.setError("Unos obavezan");return;}

        String ast = etAST.getText().toString();if (TextUtils.isEmpty(ast)){etAST.setError("Unos obavezan");return;}

        String alt = etALT.getText().toString();if (TextUtils.isEmpty(alt)){etALT.setError("Unos obavezan");return;}

        String ggt = etGGT.getText().toString();if (TextUtils.isEmpty(ggt)){etGGT.setError("Unos obavezan");return;}


        final Check check = new Check(
                personName,
                datum,
                spol,
                dob,
                sedimantacijaEritrocita,
                leukociti,
                eritrociti,
                hemoglobin,
                hematokrit,
                mch,
                mchc,
                mcv,
                trombociti,
                rdw,
                voltrombocita,
                glukoza,
                urea,
                kreatinin,
                ast,
                alt,
                ggt
        );
        databaseReference.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        if (user == null) {


                            Toast.makeText(CheckActivity.this,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {

                            writeNewCheck(userId, user, check);
                            CheckActivity.this.finish();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                        setEditingEnabled(true);
                    }
                });

    }

}