package com.example.zavrsni_rad.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.zavrsni_rad.Models.User;
import com.example.zavrsni_rad.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterFragment extends Fragment implements View.OnClickListener {

    private TextInputEditText Email, Password;
    private Button Submit;
    private FirebaseAuth auth;
    private DatabaseReference databaseReference;

    View view;

    public RegisterFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        databaseReference = FirebaseDatabase.getInstance().getReference();

        view = inflater.inflate(R.layout.register_fragment, container, false);
        Email = (TextInputEditText) view.findViewById(R.id.tiEmail);
        Password = (TextInputEditText) view.findViewById(R.id.tiPassword);
        Submit = (Button) view.findViewById(R.id.bSubmit);

        Submit.setOnClickListener(this);
        auth = FirebaseAuth.getInstance();
        return view;
    }
    @Override
    public void onClick(View view) {

        String email = Email.getText().toString().trim();
        String password = Password.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Email.setError("Unesite e-mail");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Password.setError("Unesite lozinku");
            return;
        }
        if (password.length() < 6) {
            Password.setError("Lozinka prekratka, unesite minimalno 6 znakova");
            return;
        }

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {


                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Toast.makeText(getActivity(), "Registracija uspješna", Toast.LENGTH_LONG).show();

                        if (!task.isSuccessful()) {
                            Toast.makeText(getActivity(), "Authentication failed." + task.getException(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            onAuthSuccess(task.getResult().getUser());
                        }
                    }

                });
    }

    private void onAuthSuccess(FirebaseUser user) {
        String username = usernameFromEmail(user.getEmail());


        writeNewUser(user.getUid(), username, user.getEmail());

        Toast.makeText(getActivity(), "Registration complete", Toast.LENGTH_LONG).show();

    }

    private String usernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }

    private void writeNewUser(String userId, String name, String email) {
        User user = new User(email);

        databaseReference.child("users").child(userId).setValue(user);
    }


}
