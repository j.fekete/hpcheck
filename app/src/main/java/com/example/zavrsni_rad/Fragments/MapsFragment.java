package com.example.zavrsni_rad.Fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.zavrsni_rad.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Map;

public class MapsFragment extends Fragment {

    MapView mMapView;
    private GoogleMap googleMap;

    View view;

    public MapsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment, container, false);



        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();



        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                boolean succes = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.style));

                LatLng osijek = new LatLng(45.55111 , 18.693891);
                googleMap.addMarker(new MarkerOptions().position(osijek)
                        .title("Grad Osijek").snippet(" " +
                                "Grad na Dravi <3").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                LatLng Spec_bolnica_Agram = new LatLng(45.555954, 18.693843);
                googleMap.addMarker(new MarkerOptions().position(Spec_bolnica_Agram)
                        .title("Specijalna bolnica-AGRAM").snippet("Vukovarska ul. 31, 31000, Osijek")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));


                LatLng Poliklinika_Slavonija = new LatLng(45.562969, 18.656543);
                googleMap.addMarker(new MarkerOptions().position(Poliklinika_Slavonija)
                        .title("Poliklinika Slavonija").snippet("Prominska ul., 31000, Osijek")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                LatLng Spiranovic = new LatLng(45.554185, 18.682985);
                googleMap.addMarker(new MarkerOptions().position(Spiranovic)
                        .title("Ustanova za zdravstvenu skrb dr. Špiranović").snippet(" Ul. Stjepana Radića 54, 31000, Osijek")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                LatLng Medikol = new LatLng(45.563205, 18.660202);
                googleMap.addMarker(new MarkerOptions().position(Medikol)
                        .title("Medikol Poliklinika").snippet("Ul. Josipa Jurja Strossmayera 141, 31000, Osijek")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                LatLng dr_margaretić = new LatLng(45.562555, 18.674639);
                googleMap.addMarker(new MarkerOptions().position(dr_margaretić)
                        .title("DAMIR MARGARETIĆ dr. med. PRIVATNA SPECIJALISTIČKA RADIOLOŠKO-ONKOLOŠKA ORDINACIJA").snippet("14, Ul. Josipa Jurja Strossmayera, 31000, Osijek")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));




                googleMap.moveCamera(CameraUpdateFactory.newLatLng(osijek));

                CameraPosition cameraPosition = new CameraPosition.Builder().target(osijek).zoom(13).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                        (getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                googleMap.setMyLocationEnabled(true);





            }
        });

        return view;}



    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
