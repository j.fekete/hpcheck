package com.example.zavrsni_rad.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.zavrsni_rad.ContentActivity;
import com.example.zavrsni_rad.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginFragment extends Fragment implements View.OnClickListener {
    private TextInputEditText Email, Password;
    private Button Login;
    private FirebaseAuth auth;

    View view;

    public LoginFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_fragment, container, false);
        auth = FirebaseAuth.getInstance();

        Email = (TextInputEditText) view.findViewById(R.id.etEmail);
        Password = (TextInputEditText) view.findViewById(R.id.etPassword);
        Login = (Button) view.findViewById(R.id.bLogin);

        Login.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View view) {
        String email = Email.getText().toString().trim();
        final String password = Password.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Email.setError("Unesite e-mail adresu");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Password.setError("Unesite lozinku!");
            return;
        }

        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (!task.isSuccessful()) {
                            // there was an error
                            if (password.length() < 6) {
                                Password.setError("Kriva lozinka, molim unesite točnu lozinku");
                            } else {
                                Toast.makeText(getActivity(), "Login failed", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Intent intent = new Intent(getActivity(), ContentActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }

                });
    }
}
