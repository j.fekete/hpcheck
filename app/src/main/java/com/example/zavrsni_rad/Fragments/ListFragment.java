package com.example.zavrsni_rad.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.example.zavrsni_rad.CheckActivity;
import com.example.zavrsni_rad.MainActivity;
import com.example.zavrsni_rad.Models.Check;
import com.example.zavrsni_rad.R;
import com.example.zavrsni_rad.ResultsActivity;
import com.example.zavrsni_rad.Viewholders.CheckViewholder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListFragment extends BaseFragment {


    private RecyclerView rvList;
    private Button LogOut;
    private FloatingActionButton StartCheck;
    private DatabaseReference databaseReference;
    private RecyclerView recyclerView;
    List<Check> data;
    private LinearLayoutManager mManager;
    private FirebaseRecyclerAdapter<Check, CheckViewholder> checksAdapter;

    View view;

    public ListFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.list_fragment, container, false);

        LogOut = (Button) view.findViewById(R.id.bLogOut);
        LogOut.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {
                                          FirebaseAuth.getInstance().signOut();
                                          startActivity(new Intent(getActivity(), MainActivity.class));
                                      }


                                  }

        );


        StartCheck = (FloatingActionButton) view.findViewById(R.id.fabStart);
        StartCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CheckActivity.class));
            }
        });
        recyclerView = view.findViewById(R.id.rvList);


        databaseReference = FirebaseDatabase.getInstance().getReference();

        setupUI();


        rvList = view.findViewById(R.id.rvList);


        return view;
    }


    private void setupUI() {
        mManager = new LinearLayoutManager(this.getContext());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mManager);
        Query tasksQuery = getQuery();

        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<Check>()
                .setQuery(tasksQuery, Check.class)
                .build();

        checksAdapter = new FirebaseRecyclerAdapter<Check, CheckViewholder>(options) {
            @Override
            public CheckViewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());


                return new CheckViewholder(inflater.inflate(R.layout.item_check_list, viewGroup, false));

            }

            @Override
            protected void onBindViewHolder(@NonNull CheckViewholder holder, final int position, @NonNull final Check model) {
                holder.bindToPost(model);

                holder.item_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ListFragment.this.getContext(), ResultsActivity.class);
                        intent.putExtra("CheckResult", model);
                        startActivity(intent);
                    }
                });
            }
        };
        recyclerView.setAdapter(checksAdapter);
    }


    @Override
    public void onStart() {
        super.onStart();
        if (checksAdapter != null) {
            checksAdapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (checksAdapter != null) {
            checksAdapter.stopListening();
        }
    }

    public Query getQuery() {
        return databaseReference.child("user-checks")
                .child(getUid());
    }


}