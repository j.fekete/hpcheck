package com.example.zavrsni_rad;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.zavrsni_rad.Fragments.LoginFragment;
import com.example.zavrsni_rad.Fragments.RegisterFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.tablayout_id)
    TabLayout tablayout;
    @BindView(R.id.viewpager_id)
    ViewPager viewPager;
    @BindView(R.id.appbarid)
    AppBarLayout appBarLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.AddFragment(new LoginFragment(), "Prijava");
        adapter.AddFragment(new RegisterFragment(), "Registracija");
        viewPager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewPager);
    }
}