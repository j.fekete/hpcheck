package com.example.zavrsni_rad;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.zavrsni_rad.Fragments.ListFragment;
import com.example.zavrsni_rad.Fragments.MapsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContentActivity extends AppCompatActivity {
    @BindView(R.id.tablayout_id_content)
    TabLayout tablayout_content;
    @BindView(R.id.viewpager_id_content)
    ViewPager viewPager_content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_activity);
        ButterKnife.bind(this);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.AddFragment(new ListFragment(), "Pregledi");
        adapter.AddFragment(new MapsFragment(), "Mapa");
        viewPager_content.setAdapter(adapter);
        tablayout_content.setupWithViewPager(viewPager_content);
    }}
