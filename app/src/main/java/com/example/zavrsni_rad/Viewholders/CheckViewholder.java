package com.example.zavrsni_rad.Viewholders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zavrsni_rad.Models.Check;
import com.example.zavrsni_rad.R;

import java.util.ArrayList;
import java.util.List;

public class CheckViewholder extends RecyclerView.ViewHolder {
    public TextView tvName;
    public TextView tvDate;
    public TextView tvSpol;
    public TextView tvDob;
    public LinearLayout item_layout;



    public CheckViewholder(View itemView) {
        super(itemView);


        tvName = itemView.findViewById(R.id.tvName);
        tvDate = itemView.findViewById(R.id.tvDate);
        tvSpol = itemView.findViewById(R.id.tv_spol);
        tvDob = itemView.findViewById(R.id.tv_dob);
    }

    public void bindToPost(Check check) {
        tvName.setText(check.getPersonName());
        tvDate.setText(check.getDatum());
        tvSpol.setText(check.getSpol());
        tvDob.setText(check.getDob());

        item_layout = itemView.findViewById(R.id.itemLayout);
    }


}
