package com.example.zavrsni_rad.Models;

import android.util.Log;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

public class Check implements Serializable {
    private String PersonName;
    private String Datum;
    private String uid;
    private String Spol;
    private String Dob;
    private String sedimantacijaEritrocita;
    private String Leukociti;
    private String Eritrociti;
    private String Hemoglobin;
    private String Hematokrit;
    private String MCH;
    private String MCHC;
    private String MCV;
    private String Trombociti;
    private String RDW;
    private String volTrombocita;
    private String Glukoza;
    private String Urea;
    private String Kreatinin;
    private String AST;
    private String ALT;
    private String GGT;


    public Check() { }

    public Check(String personName, String datum, String spol, String dob, String sedimantacijaEritrocita,
                 String leukociti, String eritrociti, String hemoglobin, String hematokrit, String MCH,
                 String MCHC, String MCV, String trombociti, String RDW, String volTrombocita, String glukoza,
                 String urea, String kreatinin, String AST, String ALT, String GGT) {
        PersonName = personName;
        Datum = datum;
        Spol = spol;
        Dob = dob;
        this.sedimantacijaEritrocita = sedimantacijaEritrocita;
        Leukociti = leukociti;
        Eritrociti = eritrociti;
        Hemoglobin = hemoglobin;
        Hematokrit = hematokrit;
        this.MCH = MCH;
        this.MCHC = MCHC;
        this.MCV = MCV;
        Trombociti = trombociti;
        this.RDW = RDW;
        this.volTrombocita = volTrombocita;
        Glukoza = glukoza;
        Urea = urea;
        Kreatinin = kreatinin;
        this.AST = AST;
        this.ALT = ALT;
        this.GGT = GGT;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("PersonName", PersonName);
        result.put("Datum", Datum);
        result.put("Spol", Spol);
        result.put("Dob", Dob);
        result.put("sedimantacijaEritrocita", sedimantacijaEritrocita);
        result.put("Leukociti", Leukociti);
        result.put("Eritrociti", Eritrociti);
        result.put("Hemoglobin", Hemoglobin);
        result.put("Hematokrit", Hematokrit);
        result.put("MCH", MCH);
        result.put("MCHC", MCHC);
        result.put("MCV", MCV);
        result.put("Trombociti", Trombociti);
        result.put("RDW", RDW);
        result.put("volTrombocita", volTrombocita);
        result.put("Glukoza", Glukoza);
        result.put("Urea", Urea);
        result.put("Kreatinin", Kreatinin);
        result.put("AST", AST);
        result.put("ALT", ALT);
        result.put("GGT", GGT);
        return result;
    }

    public String getPersonName() {
        return PersonName;
    }

    public String getDatum() {
        return Datum;
    }

    public String getUid() {
        return uid;
    }

    public String getSpol() {
        return Spol;
    }

    public String getDob() {
        return Dob;
    }

    public String getSedimantacijaEritrocita() {
        return sedimantacijaEritrocita;
    }

    public String getLeukociti() {
        return Leukociti;
    }

    public String getEritrociti() {
        return Eritrociti;
    }

    public String getHemoglobin() {
        return Hemoglobin;
    }

    public String getHematokrit() {
        return Hematokrit;
    }

    public String getMCH() {
        return MCH;
    }

    public String getMCHC() {
        return MCHC;
    }

    public String getMCV() {
        return MCV;
    }

    public String getTrombociti() {
        return Trombociti;
    }

    public String getRDW() {
        return RDW;
    }

    public String getVolTrombocita() {
        return volTrombocita;
    }

    public String getGlukoza() {
        return Glukoza;
    }

    public String getUrea() {
        return Urea;
    }

    public String getKreatinin() {
        return Kreatinin;
    }

    public String getAST() {
        return AST;
    }

    public String getALT() {
        return ALT;
    }

    public String getGGT() {
        return GGT;
    }

    public Boolean isSedimentacijaEritrocitaOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Integer sedimentacijaEritrocita = Integer.valueOf(this.getSedimantacijaEritrocita());
        if (this.getSpol().equals("Muško")) {
            if (dob <= 50) {
                if (
                        sedimentacijaEritrocita >= 2
                        && sedimentacijaEritrocita <= 13
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            } else {
                if (
                        sedimentacijaEritrocita >= 3
                        && sedimentacijaEritrocita <= 23
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob <= 50) {
                if (
                        sedimentacijaEritrocita >= 4
                        && sedimentacijaEritrocita <= 24
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            } else {
                if (
                        sedimentacijaEritrocita >= 5
                        && sedimentacijaEritrocita <= 28
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
    }
    public Boolean isLeukocitiOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double leukociti = Double.valueOf(this.getLeukociti());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        leukociti >= 3.4
                                && leukociti <= 9.7
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        leukociti >= 3.4
                                && leukociti <= 9.7
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
   return Boolean.TRUE; }

    public Boolean isEritrocitiOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double eritrociti = Double.valueOf(this.getEritrociti());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        eritrociti >= 4.34
                                && eritrociti <= 5.72
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        eritrociti >= 3.86
                                && eritrociti <= 5.08
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }
    public Boolean isHemoglobinOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Integer hemoglobin = Integer.valueOf(this.getHemoglobin());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        hemoglobin >= 138
                                && hemoglobin <= 175
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        hemoglobin >= 119
                                && hemoglobin <= 157
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isHematokritOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double hematokrit = Double.valueOf(this.getHematokrit());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        hematokrit >= 0.415
                                && hematokrit <= 0.530
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        hematokrit >= 0.356
                                && hematokrit <= 0.470
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isMCHOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double mch = Double.valueOf(this.getMCH());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        mch >= 27.4
                                && mch <= 33.9
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        mch >= 27.4
                                && mch <= 33.9
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isMCHCOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double mchc = Double.valueOf(this.getMCHC());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        mchc >= 320
                                && mchc <= 345
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        mchc >= 320
                                && mchc <= 345
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isMCVOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double mcv = Double.valueOf(this.getMCV());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        mcv >= 83.0
                                && mcv <= 97.2
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        mcv >= 83.0
                                && mcv <= 97.2
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isTrombocitOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double trombocit = Double.valueOf(this.getTrombociti());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        trombocit >= 158
                                && trombocit <= 424
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        trombocit >= 158
                                && trombocit <= 424
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isRDWOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double rdw = Double.valueOf(this.getRDW());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        rdw >= 9.0
                                && rdw <= 15.0
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        rdw >= 9.0
                                && rdw <= 15.0
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isProsVolTrombocitaOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double prosvoltrombocita = Double.valueOf(this.getVolTrombocita());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        prosvoltrombocita >= 6.8
                                && prosvoltrombocita <= 10.4
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        prosvoltrombocita >= 6.8
                                && prosvoltrombocita <= 10.4
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isGlukozaOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double glukoza = Double.valueOf(this.getGlukoza());
        if (this.getSpol().equals("Muško")) {
            if (dob <= 30) {
                if (
                        glukoza >= 4.2
                                && glukoza <= 6.0
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            } else {
                if (
                        glukoza >= 4.4
                                && glukoza <= 6.4
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob <= 50) {
                if (
                        glukoza >= 4.2
                                && glukoza <= 6.0
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            } else {
                if (
                        glukoza >= 4.4
                                && glukoza <= 6.4
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
    }
    public Boolean isUreaOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double urea = Double.valueOf(this.getUrea());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        urea >= 2.8
                                && urea <= 8.3
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        urea >= 2.8
                                && urea <= 8.3
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isKreatininOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double kreatinin = Double.valueOf(this.getKreatinin());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        kreatinin >= 79
                                && kreatinin <= 125
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        kreatinin >= 63
                                && kreatinin <= 107
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isALTOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double alt = Double.valueOf(this.getALT());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        alt >= 12
                                && alt <= 48
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        alt >= 10
                                && alt <= 36
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

    public Boolean isASTOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double ast = Double.valueOf(this.getAST());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        ast >= 11
                                && ast <= 38
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        ast >= 8
                                && ast <= 30
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }


    public Boolean isGGTOk() {
        Integer dob = Integer.valueOf(this.Dob);
        Double ggt = Double.valueOf(this.getGGT());
        if (this.getSpol().equals("Muško")) {
            if (dob >= 20) {
                if (
                        ggt >= 11
                                && ggt <= 55
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else if(this.getSpol().equals("Žensko")) {
            if (dob >= 20) {
                if (
                        ggt >= 9
                                && ggt <= 35
                ) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } else {
            
            return Boolean.FALSE;
        }
        return Boolean.TRUE; }

}