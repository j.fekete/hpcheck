package com.example.zavrsni_rad;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.zavrsni_rad.Models.Check;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultsActivity extends Activity {


    @BindView(R.id.tv_sedimentacijaEritrocita)
    TextView tvSedimentacijaEritrocita;
    @BindView(R.id.ivSedimentacijaEritrocita)
    ImageView ivSedimentacijaEritrocita;
    @BindView(R.id.tv_Leukociti)
    TextView tvLeukociti;
    @BindView(R.id.ivLeukociti)
    ImageView ivLeukociti;
    @BindView(R.id.tv_Eritrociti)
    TextView tvEritrociti;
    @BindView(R.id.ivEritrociti)
    ImageView ivEritrociti;
    @BindView(R.id.tv_Hemoglobin)
    TextView tvHemoglobin;
    @BindView(R.id.ivHemoglobin)
    ImageView ivHemoglobin;
    @BindView(R.id.tv_Hematokrit)
    TextView tvHematokrit;
    @BindView(R.id.ivHematokrit)
    ImageView ivHematokrit;
    @BindView(R.id.tv_MCH)
    TextView tvMCH;
    @BindView(R.id.ivMCH)
    ImageView ivMCH;
    @BindView(R.id.tv_MCHC)
    TextView tvMCHC;
    @BindView(R.id.ivMCHC)
    ImageView ivMCHC;
    @BindView(R.id.tv_MCV)
    TextView tvMCV;
    @BindView(R.id.ivMCV)
    ImageView ivMCV;
    @BindView(R.id.tv_Trombociti)
    TextView tvTrombociti;
    @BindView(R.id.ivTrombociti)
    ImageView ivTrombociti;
    @BindView(R.id.tv_RDW)
    TextView tvRDW;
    @BindView(R.id.ivRDW)
    ImageView ivRDW;
    @BindView(R.id.tv_ProsjecniVolTromb)
    TextView tvProsjecniVolTromb;
    @BindView(R.id.ivProsjecniVolTromb)
    ImageView ivProsjecniVolTromb;
    @BindView(R.id.tv_Glukoza)
    TextView tvGlukoza;
    @BindView(R.id.ivGlukoza)
    ImageView ivGlukoza;
    @BindView(R.id.tv_Urea)
    TextView tvUrea;
    @BindView(R.id.ivUrea)
    ImageView ivUrea;
    @BindView(R.id.tv_Kreatinin)
    TextView tvKreatinin;
    @BindView(R.id.ivKretinin)
    ImageView ivKretinin;
    @BindView(R.id.tv_AST)
    TextView tvAST;
    @BindView(R.id.ivAST)
    ImageView ivAST;
    @BindView(R.id.tv_ALT)
    TextView tvALT;
    @BindView(R.id.ivALT)
    ImageView ivALT;
    @BindView(R.id.tv_GGT)
    TextView tvGGT;
    @BindView(R.id.ivGGT)
    ImageView ivGGT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        ButterKnife.bind(this);
        Check check = (Check) getIntent().getSerializableExtra("CheckResult");

        setupUI(check);
    }

    private void setupUI(Check check) {
        tvSedimentacijaEritrocita.setText(check.getSedimantacijaEritrocita());
        if (check.isSedimentacijaEritrocitaOk()) {
            ivSedimentacijaEritrocita.setImageResource(R.drawable.positive);
        } else {
            ivSedimentacijaEritrocita.setImageResource(R.drawable.negative);
        }
        tvLeukociti.setText(check.getLeukociti());
        if (check.isLeukocitiOk()){
            ivLeukociti.setImageResource(R.drawable.positive);
        }else {
            ivLeukociti.setImageResource(R.drawable.negative);
        }
        tvEritrociti.setText(check.getEritrociti());
        if (check.isEritrocitiOk()){
            ivEritrociti.setImageResource(R.drawable.positive);
        }else {
            ivEritrociti.setImageResource(R.drawable.negative);
        }
        tvHemoglobin.setText(check.getHemoglobin());
        if (check.isHemoglobinOk()){
            ivHemoglobin.setImageResource(R.drawable.positive);
        }else {
            ivHemoglobin.setImageResource(R.drawable.negative);
        }
        tvHematokrit.setText(check.getHematokrit());
        if (check.isHematokritOk()){
            ivHematokrit.setImageResource(R.drawable.positive);
        }else {
            ivHematokrit.setImageResource(R.drawable.negative);
        }
        tvMCH.setText(check.getMCH());
        if (check.isMCHOk()){
            ivMCH.setImageResource(R.drawable.positive);
        }else {
            ivMCH.setImageResource(R.drawable.negative);
        }
        tvMCHC.setText(check.getMCHC());
        if (check.isMCHCOk()){
            ivMCHC.setImageResource(R.drawable.positive);
        }else {
            ivMCHC.setImageResource(R.drawable.negative);
        }
        tvMCV.setText(check.getMCV());
        if (check.isMCVOk()){
            ivMCV.setImageResource(R.drawable.positive);
        }else {
            ivMCV.setImageResource(R.drawable.negative);
        }
        tvTrombociti.setText(check.getTrombociti());
        if (check.isTrombocitOk()){
            ivTrombociti.setImageResource(R.drawable.positive);
        }else {
            ivTrombociti.setImageResource(R.drawable.negative);
        }
        tvRDW.setText(check.getRDW());
        if (check.isRDWOk()){
            ivRDW.setImageResource(R.drawable.positive);
        }else {
            ivRDW.setImageResource(R.drawable.negative);
        }
        tvProsjecniVolTromb.setText(check.getVolTrombocita());
        if (check.isProsVolTrombocitaOk()){
            ivProsjecniVolTromb.setImageResource(R.drawable.positive);
        }else {
            ivProsjecniVolTromb.setImageResource(R.drawable.negative);
        }
        tvGlukoza.setText(check.getGlukoza());
        if (check.isGlukozaOk()){
            ivGlukoza.setImageResource(R.drawable.positive);
        }else {
            ivGlukoza.setImageResource(R.drawable.negative);
        }
        tvUrea.setText(check.getUrea());
        if (check.isUreaOk()){
            ivUrea.setImageResource(R.drawable.positive);
        }else {
            ivUrea.setImageResource(R.drawable.negative);
        }
        tvKreatinin.setText(check.getKreatinin());
        if (check.isKreatininOk()){
            ivKretinin.setImageResource(R.drawable.positive);
        }else {
            ivKretinin.setImageResource(R.drawable.negative);
        }
        tvALT.setText(check.getALT());
        if (check.isALTOk()){
            ivALT.setImageResource(R.drawable.positive);
        }else {
            ivALT.setImageResource(R.drawable.negative);
        }
        tvAST.setText(check.getAST());
        if (check.isASTOk()){
            ivAST.setImageResource(R.drawable.positive);
        }else {
            ivAST.setImageResource(R.drawable.negative);
        }
        tvGGT.setText(check.getGGT());
        if (check.isASTOk()){
            ivGGT.setImageResource(R.drawable.positive);
        }else {
            ivGGT.setImageResource(R.drawable.negative);
        }



    }

}
